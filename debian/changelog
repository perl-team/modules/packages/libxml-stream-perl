libxml-stream-perl (1.24-5) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.

  [ gregor herrmann ]
  * Add Set_SSL_verifycn_name_parameter_to_fix_hostname_verification.patch
    to adjust to IO::Socket::SSL >= 2.078.
    Thanks to Manfred Stock for the bug report and the patch.
    (Closes: #1064058)
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Feb 2024 01:32:26 +0100

libxml-stream-perl (1.24-4) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Add patch 0001-Do-not-die-when-hostname-cannot-be-resolved.patch.
    Don't die if resolving the local hostname fails.
    Thanks to Thadeu Lima de Souza Cascardo for the patch. (Closes: #692311)
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * Disable DNS queries during tests.

 -- gregor herrmann <gregoa@debian.org>  Wed, 20 Jan 2021 18:34:25 +0100

libxml-stream-perl (1.24-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 18 Jan 2021 00:00:13 +0100

libxml-stream-perl (1.24-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * debian/control: Remove Franck Joncourt from Uploaders.
    Thanks to Tobias Frost (Closes: #831319)

  [ gregor herrmann ]
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Remove Jay Bonci from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Disable network tests during autopkgtests like during build.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Provide a default CA path (closes: #908027, LP: 1774614)
  * Update copyright years
  * Declare compliance with Debian Policy 4.2.1

 -- Florian Schlichting <fsfs@debian.org>  Mon, 10 Sep 2018 21:14:52 +0200

libxml-stream-perl (1.24-2) unstable; urgency=medium

  * Team upload.
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Fri, 05 Jun 2015 23:03:56 +0300

libxml-stream-perl (1.24-1) unstable; urgency=low

  [ Franck Joncourt ]
  * Renamed gssapi.patch to t_upstream_uninitialized_value.diff, and refreshed
    the headers.
  * Added /me to Uploaders. Refreshed d.control and d.copyright accordingly.
  * Switch to dpkg-source 3.0 (quilt) format.
    + Removed useless README.source which only documented the quilt usage.
    + Removed --with-quilt argument for debhelper.
    + Removed BDI on quilt in d.control.

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Fabrizio Regalli ]
  * Add myself to Uploaders and Copyright.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 1.24
  * Update debian/copyright (add current author, fix license, drop paragraphs
    for removed embedded code copies)
  * Drop patches applied upstream, refresh and forward remaining patch
  * Declare compliance with Debian Policy 3.9.6
  * Add myself to uploaders and copyright
  * No more fiddling with t/lib in debian/rules (dropped upstream)
  * Add autopkgtest header

 -- Florian Schlichting <fsfs@debian.org>  Sun, 03 May 2015 23:37:32 +0200

libxml-stream-perl (1.23-2) unstable; urgency=low

  * Team upload.
  * Add 687059_Use-of-uninitialized-value.patch (closes: #687059).

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 04 Nov 2012 22:10:09 +0100

libxml-stream-perl (1.23-1) unstable; urgency=low

  * New upstream release
  * Update copyright information and clauses
  * Remove resolver.patch, applied upstream
  * Refresh parts of gssapi.patch not applied upstream
  * Add myself to uploaders and Copyright
  * Rewrite control description

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 09 Jan 2010 12:49:51 -0500

libxml-stream-perl (1.22-4) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza).
  * Set Maintainer to Debian Perl Group.
  * Use dist-based URL in debian/watch.
  * Split out changes in lib/XML/Stream.pm into patches; add quilt framework.
  * don't install README any more.
  * debian/control: move Suggests from source to binary stanza.
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * debian/control: Remove duplicate Section, Priority fields from binary
    package stanza.
  * debian/control: Mention module name in description.
  * Bump Standards-Version to 3.8.3.
  * Refresh rules for debhelper 7.
  * Add myself to Uploaders.
  * debian/watch: Use extended regular expression to match upstream releases.
  * debian/rules: Move t/lib aside while running tests to use Test::Simple
    bundled with perl instead of a local copy.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 03 Jan 2010 20:52:36 +0900

libxml-stream-perl (1.22-3) unstable; urgency=low

  * Thanks to Andreas for the upload (Closes: #427071)
  * Added Suggests: libnet-dns-perl
  * Accepted patch from Pablo Barbachano. Thanks! (Closes: #395395)
  * Updated debian/watch so that uupdate will work
  * Policy-version bump to 3.7.2.2 (no other changes)
  * Updated debhelper into Build-Depends instead of -Indep for lintian
  * Only conditionally ignore make clean, also for lintian

 -- Jay Bonci <jaybonci@debian.org>  Sat, 07 Jul 2007 17:53:40 -0400

libxml-stream-perl (1.22-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix GSSAPI SASL authentication. Closes: #427071

 -- Andreas Barth <aba@not.so.argh.org>  Tue,  3 Jul 2007 20:24:26 +0000

libxml-stream-perl (1.22-2) unstable; urgency=low

  * Fixes FTBFS. (Build) Dep on libauthen-sasl-perl (Closes: #276283)

 -- Jay Bonci <jaybonci@debian.org>  Wed, 13 Oct 2004 03:44:13 -0400

libxml-stream-perl (1.22-1) unstable; urgency=low

  * New upstream version (Closes: #272847)
  * Adds debian/watch file so uscan will actually work

 -- Jay Bonci <jaybonci@debian.org>  Thu, 30 Sep 2004 10:27:21 -0400

libxml-stream-perl (1.17-1) unstable; urgency=low

  * New upstream version (Closes: #215615)
  * New Maintainer (Closes: #210545)
  * Clarified license as GPL-only
  * Updated to policy version 3.6.1.0 (no other changes)
  * Removed install file as it is no longer needed

 -- Jay Bonci <jaybonci@debian.org>  Mon, 15 Mar 2004 15:31:53 -0500

libxml-stream-perl (1.16-2) unstable; urgency=low

  * debian/rules: moved debhelper compatibility level setting to
    'debian/compat' per latest debhelper best practices
  * debian/control: updated sections according to latest archive changes:
    - 'libxml-stream-perl' from 'interpreters' to 'perl'
  * debian/control: upgraded build dependency on 'debhelper' to '>= 4.1'
  * debian/control: upgraded to Debian Policy 3.6.0 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Mon,  1 Sep 2003 12:06:16 -0500

libxml-stream-perl (1.16-1) unstable; urgency=low

  * New upstream release
    - skip tests requiring Internet connection if none present
      (closes: Bug#165170)
  * debian/control: removed obsolete (build) dependency on
    'libunicode-string-perl'
  * debian/control: upgraded to Debian Policy 3.5.7 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun, 10 Nov 2002 19:27:58 -0600

libxml-stream-perl (1.15-2) unstable; urgency=low

  * debian/rules: upgraded to debhelper v4
  * debian/control: changed build dependency on debhelper accordingly
  * debian/rules: migrated from 'dh_movefiles' to 'dh_install'
  * debian/rules: split off 'install' target from 'binary-indep' target
  * debian/copyright: added pointer to license
    (closes: Bug#157679)
  * debian/control: added (build) dependency on 'libio-socket-ssl-perl'

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 21 Sep 2002 13:29:05 -0500

libxml-stream-perl (1.15-1) unstable; urgency=low

  * New upstream release

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 15 Jun 2002 12:58:28 -0500

libxml-stream-perl (1.14-2) unstable; urgency=low

  * Stream/Parser.pm: applied another patch to silence perl when running
    with the '-w' option
    (closes: Bug#125745) (thanks Michael)

 -- Ardo van Rangelrooij <ardo@debian.org>  Fri, 21 Dec 2001 19:57:40 -0600

libxml-stream-perl (1.14-1) unstable; urgency=low

  * New upstream release

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun, 11 Nov 2001 10:56:33 -0600

libxml-stream-perl (1.13-1) unstable; urgency=low

  * New upstream release
    (closes: Bug#118020)
  * Stream/Parser.pm: applied patch to silence perl when running with the
    '-w' option
    (closes: Bug#118029) (thanks Michael)
  * debian/control: removed obsolete (build) dependency on
    'libxml-parser-perl'
  * debian/control: added missing (build) dependency on
    'libunicode-string-perl'
  * debian/control: upgraded to Debian Policy 3.5.6

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat,  3 Nov 2001 13:43:54 -0600

libxml-stream-perl (1.12-2) unstable; urgency=low

  * debian/control: upgraded to Debian Policy 3.5.5
  * debian/control: upgraded to Debian Perl Policy 1.20

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun,  2 Sep 2001 17:52:34 -0500

libxml-stream-perl (1.12-1) unstable; urgency=low

  * New upstream release

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun, 27 May 2001 22:53:59 -0500

libxml-stream-perl (1.11-1) unstable; urgency=low

  * New upstream release
  * debian/control: updated 'debhelper' dependency to remove 'dh_testversion'
  * debian/control: upgraded to Debian Policy 3.5.2
  * debian/control: upgraded to Debian Perl Policy 1.17
  * debian/rules: upgraded to Debian Perl Policy 1.17

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun, 15 Apr 2001 19:24:58 -0500

libxml-stream-perl (1.10-1) unstable; urgency=low

  * New upstream release
  * debian/control: upgraded to Debian Policy 3.2.1
  * debian/rules: upgraded to debhelper v3

 -- Ardo van Rangelrooij <ardo@debian.org>  Wed, 17 Jan 2001 13:57:34 -0600

libxml-stream-perl (1.09-1) unstable; urgency=low

  * New upstream release

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 14 Oct 2000 19:28:03 -0500

libxml-stream-perl (1.05-1) unstable; urgency=low

  * Initial Release

 -- Ardo van Rangelrooij <ardo@debian.org>  Fri, 21 Jul 2000 11:39:59 +0200
